# 基本SQL语法

## 1. SQL概述

### 1.1 SQL背景

SQL是使用关系模型的数据库应用语言，与数据直接打交道，由IBM开发，后由美国国家标准局（ANSI）制定SQL标准，先后有`SQL-86`、`SQL-89`、`SQL-92`、`SQL-99`

最重要的两个标准是SQL92和SQL99，现在的SQL依旧遵循这些标准，不同数据库生产厂商都支持SQL语句，但都有特有内容



### 1.2 SQL分类

SQL语言在功能上主要分为三大类：

+ DDL：Data Definition Languages，数据定义语言，这些定义了不同的数据库、表、视图、索引等数据库对象，还可以用来创建、删除、修改数据库和数据表的结构
  + 主要语句关键字包括`CREATE`、`DROP`、`ALTER`等
+ DML：Data Manipulation Languages，数据操作语言，用于添加、删除、更新和查询数据库记录，并检查数据完整性
  + 主要语句关键字包括`INSERT`、`DELETE`、`UPDATE`、`SELECT`等
  + SELECT是SQL语言的基础
+ DCL：Data Control Languages，数据控制语言，用于定义数据库、表、字段、用户的访问权限和安全级别
  + 主要语句关键字包括`GRANT`、`REVOKE`、`COMMIT`、`ROLLBACK`、`SAVEPOINT`等



因为查询语句使用的非常频繁，很多人把查询语句作为单独一类：DQL，数据查询语言

也有将COMMIT、ROLLBACK单独称为TCL：Transaction Control Languages，事务控制语言



## 2. SQL语言规则与规范

### 2.1 基本规则

+ SQL可以写在一行或者多行，为了提高可读性，各子句分行写，必要时使用缩进
+ 每条命令以`;`、`\g`或者`\G`结束
+ 关键字不能被缩写，也不能被分行
+ 标点符号
  + 所有的括号、单引号、双引号必须成对
  + 必须使用英文状态的半角输入方式
  + 字符串型和日期时间类型的数据可以使用单引号('')表示
  + 列的别名尽量使用双引号("")，且不建议省略as



### 2.2 SQL大小写规范

+ MySQL在Windows环境下是大小写不敏感的
+ MySQL在Linux环境下是大小写敏感的
  + 数据库名、表名、表别名、变量名是严格区分大小写的
  + 关键字、函数名、列名（字段名）、列别名（字段别名）是忽略大小写的
+ 推荐采用统一的书写规范
  + 数据库名、表名、表别名、字段名、字段别名等都小写
  + SQL关键字、函数名、绑定变量等都大写



### 2.3 注释

单行注释：# MySQL特有方式

单行注释：--

多行注释：/* */



### 2.4 命名规则

+ 数据库、表名不得超过30字符，变量名限制为29个字符
+ 只能包含A-Z、a-z、0-9、_共计63个字符
+ 数据库名、表名、字段名等对象名中间不能包含空格
+ 同一个MySQL服务中，数据库不能同名；同一个数据库中，表不能同名；同一个表中，字段不能同名
+ 字段名不能与保留字、数据库系统或常用方法冲突，如果一定要使用，在SQL语句中需要使用 `（着重号）符号引起来
+ 保持字段名与类型的一致性



### 2.5 数据导入指令

+ 命令行客户端，使用`source /path/to/.sql`命令，执行sql脚本
+ 使用图形化界面工具



## 3. DML数据操作语句

### 3.1 基本SELECT查询语句

基本语法：

```sql
SELECT field [AS Alias]
FROM table
WHERE condition;
```



去除重复行：

```sql
SELECT DISTINCT field
FROM table
WHERE condition;
```

DISTINCT关键字对整行数据进行去重，需要放在所有字段之前



空值`NULL`的处理

+ null不等同于`''`、`0`、`'null'`等
+ null值参与运算的所有结果都是null



显示表结构：

```sql
DESCRIBE talbeName;
DESC tableName;
```



### 3.2 运算符

+ 算术运算符：`+`、`-`、`*`、`/或DIV`、`%或MOD`
  + 整数类型的数据运算，结果是整数类型
  + 整数类型与浮点数类型的数据运算，结果是浮点数类型
  + 除法运算的除数为0时，运算结果为null
+ 比较运算符
  + 比较运算符的返回结果为1（结果为真）、0（结果为假）、null（有null值参与判断时返回null）
  + 符号类型运算符：`=`、`<=>`、`<>`、`!=`、`<`、`>` 、`<=`、`>=`
  + 非符号类型运算符：`IS NULL`、`IS NOT NULL`、`BETWEEN AND`、`LEAST`、`GREATEST`、`IN`、`NOT IN`、`LIKE`、`REGEXP`、`RLIKE`
  + `<=>`是安全相等，功能与`=`相同，但是可以对null值进行对比，符号两侧都是null是返回1，只有一个为null时返回0
  + `LEAST(value1, value2, ...)`返回数值最小的，`GREATEST(value1, value2, ...)`返回数值最大的
  + `LIKE`表示模糊查询，查询条件里`%`表示任意数量字符，`_`表示任意一个字符，`\`对后一个字符进行转义
  + `REGEXP`和`RLIKE`表示匹配正则表达式
+ 逻辑运算符：`NOT或!`、`AND或&&`、`OR或||`、`XOR` 
  + 逻辑运算符的返回结果为1、0、null
  + AND优先级高于OR
+ 位运算符：`&`、`|`、`^`、`~`、`>>`、`<<`
  + 位运算符用于在二进制数上进行运算，先将操作数转成二进制数，运算后再将结果转换成十进制数



### 3.3 排序与分页

+ 排序
  + 没使用排序语法时，默认排序是数据的添加顺序
  + 可以使用`ORDER BY`子句按照指定的字段顺序进行排序
  + ASC（ascend）：升序
  + DESC（descend）：降序
  + null值与任何值比较的结果都是false，所以默认排在最小的位置，即升序排在最前面，降序排在最后面
  + 没有显式指定的话默认按升序排序
  + 排序子句需要放在SELECT语句的结尾
+ 分页
  + 可以使用`LIMIT`关键字实现数据查询的分页显示
  + LIMIT [start_index,] number
    + start_index表示本次查询的起始偏移量，从第`start_index+1`行记录开始显示，省略时默认从第一行开始显示（即index=0）
    + number表示本次要显示的行数
  + 存在排序子句时，LIMIT分页子句放在排序子句的后面
  + MySQL8.0以后，新增一种格式`LIMIT number OFFSET start_index`
  + 不同的DBMS中，使用的分页关键字可能不同
    + MySQL、PostgreSQL、MairaDB、SQLite使用LIMIT
    + SQL server、Access使用TOP
    + DB2使用FETCH FIRST number ROWS ONLY
    + Oracle使用ROWNUM



### 3.4 多表查询

多表查询也称为关联查询，一起查询的表之间存在关系



+ 交叉连接：
  + SQL92和SQL99中，都使用`CROSS JOIN`表示交叉连接
  + 将任意表进行连接，即使两张表不相关
  + 交叉连接的结果，是相连接的两张表的笛卡尔积
  + 可以省略`CROSS JOIN`，直接在`FROM`后写出交叉连接的多张表，但没有连接条件
+ 内连接
  + 合并具有同一列的两个以上表的行，结果集中`不包含`一个表与另一个表不匹配的行
  + 在`FROM`关键字后写出内连接的多张表，在WHERE`关键`字后写出多表之间的连接条件
  + SQL99中，可以使用`JOIN ... ON ...`表示内连接
+ 外连接
  + 两个表在连接过程中，除了返回满足连接条件的行以外，还返回左（或右）表中不满足条件的行，没有匹配的行时，对应的列为null
  + 左外连接：返回左表中不满足条件的行，左表称为主表，右表称为从表
  + 右外连接：返回右表中不满足条件的行，右表称为主表，左表称为从表
  + 全外连接：左表和右表中不满足的行都返回，MySQL不支持全外连接
  + SQL99中，使用`LEFT JOIN ... ON ...`表示左外连接，使用`RIGHT JOIN ... ON ...`表示右外连接，使用`FULL OUTER JOIN ... ON ...`表示全外连接
  + SQL92中，使用`(+)`表示外连接，（+）前的表是从表；Oracle对SQL92的支持较好，MySQL不支持这种形式的外连接
+ 自然连接
  + SQL99在SQL92基础上提供的新语法
  + 自动查询两张连接表中的`所有相同`的字段，并进行等值连接
  + 使用`NATURAL JOIN`表示
+ USING连接
  + SQL99提供的新语法
  + 将连接表按照指定的同名字段进行等值连接，相当于对内连接的简化
  + 使用`JOIN ... ON USING(...)`表示



注：需要控制连接表的数量，多表连接相当于嵌套for循环一样，消耗资源，会使SQL查询性能下降



### 3.5 合并查询结果

可以使用`UNION`关键字，将多条SELECT语句的结果合并成一个结果集

```sql
SELECT column, ... FROM table1
UNION [ALL]
SELECT column, ... FROM table2
```

合并时，两个表对应的列数和数据类型必须相同，并相互对应

UNION：返回两个查询结果集的并集，并去除重复记录

UNION ALL：返回两个查询结果集的并集，不去重

UNION ALL执行时消耗的资源比UNION少



### 3.6 函数

函数：将经常使用的代码封装起来，需要使用时直接调用接口，提高代码效率和可维护性，函数可以分为内置函数和自定义函数

不同的DBMS之间，函数的差异性很大，大部分DBMS都有自己特定的函数，采用SQL函数的代码可移植性很差，此处介绍MySQL中的函数



+ 单行函数：数值函数、字符串函数、日期时间函数、流程控制函数、加密解密函数、MySQL信息函数
  + 流程控制函数：IF(value, value1, value2)、IFNULL(value1, value2)、CASE WHEN ... THEN ... ELSE ... END
  + 加密解密函数：PASSWORD(str)（在MYSQL8.0已弃用）、MD5(str)、SHA(str)、ENCODE(value, seed)、DECODE(value, seed)
  + MySQL信息函数：VERSION()、CONNECTION_ID()、DATABASE()、SCHEMA()、USER()、SYSTEM_USER()、CURRENT_USER()、SESSION_USER()、CHARSET(value)、COLLATION(value)
+ 聚合函数：
  + AVG()、SUM()、MAX()、MIN()、COUNT()
  + GROUP BY
  + HAVING



SQL99中SELECT语句的完整结构

```sql
SELECT ...
FROM  ... (LEFT / RIGHT) JOIN ... ON ...
WHERE ...
GROUP BY ...
HAVING ...
ORDER BY ...(ASC / DESC)
LIMIT ...,...
```

执行顺序：FROM -> ON -> WHERE -> GROUP BY -> HAVING -> SELECT -> DISTINCT -> ORDER BY -> LIMIT



### 3.7 子查询

子查询是指一个查询语句嵌套在另一个查询语句内部



子查询的分类：

+ 单行子查询与多行子查询
+ 相关子查询与非相关子查询



注意事项：

+ 子查询在主查询之前一次执行完成

+ 子查询的结果被主查询使用

+ 子查询要包含在括号内
+ 子查询放在比较条件的右侧
+ 单行操作符对应单行子查询，多行操作符(IN/ANY/ALL/SOME)对应多行子查询

子查询通常也会与EXISTS、NOT EXISTS关键字一起使用



### 3.8 新增数据

```sql
-- 新增一条数据
INSERT INTO tableName(字段1, 字段2, ...) VALUES (值1, 值2, ...);

-- 新增多条数据
INSERT INTO tableName(字段1, 字段2, ...) 
VALUES (值1, 值2, ...), (值1, 值2, ...), ...;

-- 将查询结果插入到表中
INSERT INTO tableName(字段1, 字段2, ...)
SELECT (字段1, 字段2, ...)
FROM tableName
WHERE condition;
```



### 3.9 更新数据

```sql
UPDATE tableName
SET 字段1 = 值1, 字段2 = 值2, ...
WHERE condition;
```



### 3.10 删除数据

```sql
DELETE FROM tableName
WHERE condition;
```



## 4. DDL数据定义语言

从系统架构的层次上看，MySQL数据库系统从大到小依次是`数据库服务器`、`数据库`、`数据表`、`表的行与列`



### 4.1 数据库操作

创建数据库：

```sql
CREATE DATABASE 数据库名;
CREATE DATABASE 数据库名 CHARACTER SET 字符集;
CREATE DATABASE IF NOT EXISTS 数据库名;
```



使用数据库：

```sql
-- 查看所有数据库
SHOW DATABASES;

--查看正在使用的数据库
SELECT DATABASE();

-- 查看指定库下所有的表
SHOW TABLES FROM 数据库名;

-- 查看数据库的创建信息
SHOW CREATE DATABASE 数据库名;

-- 切换数据库
USE 数据库名;
```



修改数据库：

```sql
ALTER DATABASE 数据库名 CHARACTER SET 字符集;
```



删除数据库：

```sql
DROP DATABASE 数据库名;
DROP DATABASE IF EXISTS 数据库名;
```



### 4.2 数据表操作

数据类型：

+ 整数类型：TINYINT、SMALLINT、MEDIUMINT、INT(INTEGER)、BIGINT
+ 浮点类型：FLOAT、DOUBLE
+ 定点数类型：DECIMAL
+ 位类型：BIT
+ 日期时间类型：YEAR、TIME、DATE、DATETIME、TIMESTAMP
+ 文本字符串类型：CHAR、VARCHAR、TINTTEXT、TEXT、MEDIUMTEXT、LONGTEXT
+ 枚举类型：ENUM
+ 集合类型：SET
+ 二进制字符串类型：BINARY、VARBINARY、TINYBLOB、BLOB、MEDIUMBLOB、LONGBLOB
+ JSON类型：JSON对象、JSON数组
+ 空间数据类型：
  + 单值：GEOMETRY、POINT、LINESTRING、POLYGON
  + 集合：MULTIPOINT、MULTILINESTRING、MULTIPOLYGON、GEOMETRYCOLLECTION



创建数据表：

```sql
CREATE TABLE [IF NOT EXISTS] 表名(
    字段1, 数据类型 [约束条件] [默认值],
    字段2, 数据类型 [约束条件] [默认值],
    字段3, 数据类型 [约束条件] [默认值],
    ...
    [表约束条件]
);

CREATE TABLE 表名
AS
SELECT ... FROM 表名;
```



修改数据表：

```sql
-- 增加字段
ALTER TABLE 表名 ADD [column] 字段名 字段类型 [FIRST|AFTER 字段名];

-- 修改字段
ALTER TABLE 表名 MODIFY [column] 字段名 字段类型 [DEFAULT 默认值] [FIRST|AFTER 字段名];

-- 重命名字段
ALTER TABLE 表名 CHANGE [column] 字段名 新字段名 新数据类型;

-- 删除字段
ALTER TABLE 表名 DROP [column] 字段名;

-- 重命名表
RENAME TABLE 表名 TO 新表名;
ALTER TABLE 表名 RENAME [TO] 新表名;
```



删除数据表：

```sql
DROP TABLE [IF EXISTS] 表名;
```



清空数据表：

```sql
TRUNCATE TABLE 表名;
```



MySQL8.0新特性：

创建或修改数据表时，可以使用`GENERATED ALWAYS AS ... VIRTUAL`来定义计算列

如 `c INT GENERATED ALWAYS AS (a + b) VIRTUAL`，表示c列始终等于a+b，每次操作数据时会自动计算



## 5. DCL数据控制语言

### 5.1 提交与回滚

```sql
-- 提交操作
COMMIT;

-- 回滚操作
ROLLBACK;
```

DDL语句执行后无法回滚；DML语句默认不可回滚，需要手动关闭自动提交
