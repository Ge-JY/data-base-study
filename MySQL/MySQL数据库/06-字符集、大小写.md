# 字符集

## 1. 字符集

### 1.1 默认字符集

MySQL8.0之前的版本，默认字符集为`latin1`，utf8字符集指向的是`utf8mb3`，通常需要手动将编码改为utf8字符集

MySQL8.0之后的版本，默认字符集为`uft8mb4`

```sql
SHOW VARIABLES like 'character%';
```

![image-20240228125730854](图片/image-20240228125730854.png)	



### 1.2 修改字符集

修改默认字符集变量，需要在配置文件中进行修改：

```ini
[mysqld]
character_set_server=utf8
```

修改后重启服务即可生效（只修改系统变量，不会对已创建的数据库和表产生影响）



修改已创建的数据库表的字符集：

```sql
ALTER DATABASE 数据库名 character set 'utf8';
ALTER TABLE 表名 CONVERT TO CHARACTER SET 'utf8';
```

修改表的字符集，不会对原有数据产生影响，如果原有数据使用的字符集不是utf8，需要先导出或删除，再重新插入



### 1.3 字符集的级别

MySQL有4个级别的字符集和比较规则：

+ 服务器级别
+ 数据库级别
+ 表级别
+ 列级别

```sql
SHOW VARIABLES like 'character%';

-- character_set_server：服务器级别的字符集
-- character_set_database：当前数据库的字符集
-- character_set_client：服务器解码请求时使用的字符集
-- character_set_connection：服务器处理请求时使用的字符集，在处理请求时会将请求字符串从character_set_client转为character_set_connection
-- character_set_results：服务器向客户端返回数据时使用的字符集
```



创建数据库、数据表以及列时，可以使用`CHARACTER SET`和`COLLATE`关键字指定要使用的字符集

如果没有显式指定：

+ 创建数据库时，默认使用服务器级别的字符集和比较规则
+ 创建数据表时，默认使用当前数据库的字符集和比较规则
+ 创建列时，默认使用当前数据表的字符集和比较规则



### 1.4 utf8字符集

`utf8`字符集表示一个字符需要使用1~4个字节，但常用的一些字符使用1~3个字节就足够了

字符集表示一个字符所使用的最大字节长度，在某些方面会影响系统的存储和性能

MySQL定义了两个概念：

+ utf8mb3：只使用1~3个字节表示字符的utf8字符集
+ utf8mb4：使用1~4个字节的正式的utf8字符集

在MySQL中，`utf8`指的是`utf8mb3`，如果需要使用4字节编码一个字符，如存储一些emoji表情，则需要显式指定`utf8mb4`



查看MySQL支持的字符集：

```sql
SHOW CHARSET;
SHOW CHARACTER SET;
```

<img src="图片/image-20240228131244460.png" alt="image-20240228131244460" style="zoom:80%;" />	



## 2. 比较规则

比较规则（Collation）是指对字符进行比较、排序时使用的规则

MySQL一种支持41种字符集，1.4节的图中`Default collation`列就是这种字符集的默认比较规则



比较规则的名称由三部分组成，前缀表示用在哪种字符集中，中间部分表示以哪种语言规则进行比较，后缀标识是否区分重音、大小写等

| 后缀 | 英文释义           | 描述             |
| ---- | ------------------ | ---------------- |
| _ai  | accent insensitive | 不区分重音       |
| _as  | accent sensitive   | 区分重音         |
| _ci  | case insensitive   | 不区分大小写     |
| _cs  | case sensitive     | 区分大小写       |
| _bin | binary             | 以二进制方式比较 |



常用查询操作：

```sql
-- 查看utf8字符集的比较规则
SHOW COLLATION LIKE 'utf8%';

-- 查看服务器的字符集和比较规则
SHOW VARIABLES LIKE '%_server';
```



说明：

+ utf8_unicode_ci和utf8_general_ci对中、英文没有实质的差别，utf8_unicode_ci校对速度快，utf8_general_ci准确度高，一般来说使用utf8_general_ci就足够，但如果语言涉及德语、法语或俄语等，需要使用utf8_unicode_ci
+ 修改数据库的默认字符集和比较规则，不会对已创建的表和列产生影响



## 3. 请求到响应的字符集变化

客户端往服务器发送的请求，本质上是一个`字符串`，响应数据也是一个字符串，实际上是某种字符集编码的`二进制数据`

发送请求到返回结果的过程中涉及到多次字符集的`转换`，使用到三个系统变量

+ character_set_client
+ character_set_connection
+ character_set_results

这三个变量在不同的操作系统可能会有不同的默认值



1. 客户端发送请求

   一般情况下，客户端使用的字符集与当前操作系统一致（类unix系统使用utf8，Windows系统使用gbk）

   查询`select * from t where col = '我'`

   如当前客户端使用`utf8`，发送字符`我`，在请求中的字节形式是`0xE68891`

   

2. 服务器接收请求

   服务器端接收到二进制的字节，会认为这段字节使用的字符集是`character_set_client`（假定也是utf8），然后按照utf8对字节串`0xE68891`进行解码，得到字符串`我`

   

3. 服务器处理请求

   对解码得到的字符串`我`，按照`character_set_connection`（假定是gbk）进行编码，得到字节串`0xCED2`；

   查询数据表t的col列（假定col列的字符集也是gbk），在列中找到值为`0xCED2`的记录，并对查询到的记录按照col列的字符集gbk进行编码，得到查询结果字符串

   （如果列的字符集，与character_set_connection不一样，则还需要进行一次转换）

   

4. 服务器返回请求

   服务器对查询结果的字符串，使用`character_set_results`字符集（utf8）进行编码，得到新的二进制字符串，发送给客户端

   

5. 客户端展示结果

   客户端使用的字符集是utf8，将响应字符串解码，转换成用户可以理解的内容进行展示



实际开发中，通常把character_set_client、character_set_connection、character_set_results都设置成与客户端一致的字符集，减少字符集转换，MySQL提供了简便的设置语句：

```sql
SET NAMES 字符集名;
-- 相当于以下三个语句
SET character_set_client = 字符集名;
SET character_set_connection = 字符集名;
SET character_set_results = 字符集名;
```

也可以在配置文件中，增加一个客户端的启动选项

```ini
[client]
default-character-set=utf8
```



## 4. 大小写规范

在MySQL中，列名、列别名、关键字、函数名等不区分大小写，数据库名、表名、表别名的大小写由系统参数控制

```sql
SHOW VARIABLES LIKE '%lower_case_table_names%';
```

lower_case_names参数有三个选项：0/1/2

+ 0：大小写敏感
+ 1：大小写不敏感，创建的数据库、表都以`小写形式`存放在磁盘上，对于sql语句都是转换成小写，对数据库表进行查询
+ 2：创建的数据库、表按照DDL语句中的格式存放，查询时转换为小写处理

Windows系统下，lower_case_names的默认值为1，即大小写不敏感

Linux系统下，lower_case_names的默认值为0，即大小写敏感



在Linux系统下，如果要设置大小写不敏感，需要在配置文件`my.cnf`中进行参数设置

```ini
[mysqld]
lower_case_names=1
```

注意：

+ 在MySQL5.7版本下，修改配置文件并重启数据库服务即可生效，但不会对已创建的数据库表产生影响，需要将原有的数据库和表转换为小写，否则会出现找不到数据库名
+ 在MySQL8.0版本下，`不允许`在重启服务时将lower_case_names参数值修改成`不同于初始化时`设置的值，只能在第一次启动MySQL服务前进行设置



## 5. sql_mode

sql_mode是MySQL支持的SQL语法以及执行数据验证检查的`严格程度`

MySQL5.6版本的sql_mode默认值是空，即`NO_ENGINE_SUBSTITUTION`，宽松模式

MySQL5.7版本的sql_mode默认值是`STRICT_TRANS_TABLES`，严格模式

宽松模式下，在插入数据时即使给了一个错误的数据（如超出最大长度等），也可能被接受并且不报错；严格模式下，会进行严格的数据校验，错误数据不能插入，报出错误并回滚事务



MySQL中的sql_mode有很多选项，可以参考官方文档[MySQL :: MySQL 8.3 Reference Manual :: 7.1.11 Server SQL Modes](https://dev.mysql.com/doc/refman/8.3/en/sql-mode.html)

