# 目录结构



## 1. 主要目录结构

查找MySQL目录

```bash
find / -name mysql
```



### 1.1 数据库文件存放目录

`数据目录`：/var/lib/mysql

MySQL服务运行过程中产生的数据会存储到这个目录下的文件中，启动时也会从这个目录加载文件

![image-20240228214453073](图片/image-20240228214453073.png)	



在MySQL服务中，数据目录有一个对应的系统变量`datadir`

```sql
SHOW VARIABLES LIKE 'datadir';
```



### 1.2 相关命令目录

命令目录：/usr/bin和/usr/sbin

bin目录里存储了许多关于控制客户端程序和服务器程序的命令（可执行文件，如mysql、mysqld等）



### 1.3 配置文件目录

配置文件目录：/usr/share/mysql-8.0（命令和配置文件）和/etc/my.cnf(配置文件)



## 2. 数据库与文件系统的关系

`文件系统`：操作系统用来管理磁盘的结构

存储引擎（如InnoDB、MyISAM等）将表存储在文件系统上



### 2.1 查看默认数据库

```sql
-- 查看所有数据库
SHOW DATABASES;
```

MySQL服务自带四个数据库：

+ `mysql`：MySQL系统自带的核心数据库，存储了MySQL的用户账户和权限信息，一些存储过程、事件的定义信息，一些运行过程中产生的日志信息，一些帮助信息及时区信息等
+ `information_schema`：保存MySQL服务器维护的所有其他数据库的信息，比如表、视图、触发器、列、索引等，并不记录真实的用户数据，而是一些描述性信息，也称为`元数据`
+ `performance_schema`：保存MySQL服务器运行过程中的一些状态信息，可以用来监控各类性能指标，包括统计最近执行的语句、使用的时间、内存的使用情况等
+ `sys`：通过`视图`的方式，将information_schema和performance_schema结合起来，便于监控MySQL的技术性能.



### 2.2 数据库在文件系统中的表示

创建数据库时，会在`数据目录`/var/lib/mysql目录下，创建一个与数据库名相同的子目录



#### 2.2.1 InnoDB存储引擎

MySQL5.7及以前的版本中，会在数据库子目录下创建三类文件：

+ `db.opt`：存储数据库的各种属性，如字符集和比较规则等，只有一个
+ `表名.frm`：存储表结构的相关信息，每个表有一个
+ `表名.ibd`：存储表中的数据，每个表有一个，被称为`独立表空间`

MySQL8.0及以后的版本中，在数据库子目录下只创建一类文件：

+ `表名.ibd`：存储表的字符集、比较规则、数据、结构等所有信息



在数据目录/var/lib/mysql下，还有一个`ibdata1`文件，被称为`系统表空间`，可以通过`[server] innodb_data_file_path`参数对系统表空间进行自定义

在MySQL5.5.7到5.6.6之间的版本，表数据会默认存储到系统表空间中；在MySQL5.6.6之后的版本，表数据会默认保存到独立表空间中

可以通过`[server] innodb_file_per_table`参数，人为指定数据存储到哪种表空间



Oracle公司提供了一个应用程序`ibd2sdi`，用于从.ibd文件中提取表结构信息，MySQL8.0自带此工具

```bash
ibd2sdi  --dump-file=文件名.txt 表名.ibd
```

ibd2sdi命令官方文档：[MySQL :: MySQL 8.0 Reference Manual :: 6.6.1 ibd2sdi — InnoDB Tablespace SDI Extraction Utility](https://dev.mysql.com/doc/refman/8.0/en/ibd2sdi.html)



#### 2.2.2 MyISAM存储引擎

MySQL5.7及以前的版本中，会在数据库子目录下创建三类文件：

+ `表名.frm`：存储表结构
+ `表名.MYD`：存储数据
+ `表名.MYI`：存储索引

MySQL8.0及以后的版本，也会创建三类文件：

+ `表名.sdi`：存储表结构
+ `表名.MYD`：存储数据
+ `表名.MYI`：存储索引



### 2.3 视图在文件系统中的表示

视图实际上是虚拟的表，只是查询语句的一个别名，因此存储视图时不需要存储真实的数据，只需要存储结构

视图在数据库子目录下，只存储一个`.frm`文件即可

